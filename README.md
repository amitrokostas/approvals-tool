# Approvals Tool
This script allows you to easily get and set on-chain spend approvals for ERC-20 tokens.

## Configuration
```
npm install
```

Set environment variables:
```
FIREBLOCKS_API_KEY=<Your API User ID>
FIREBLOCKS_SECRET_PATH=<Path to your API User's secret key>
```

## Getting approval
```
node script.js get_approval <Vault Account ID> <Asset ID> <Spender Address>
```

## Setting approval
```
node script.js set_approval <Vault Account ID> <Asset ID> <Spender Address> <Approval Amount>
```