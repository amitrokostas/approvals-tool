const fs = require("fs");
const { FireblocksSDK, PeerType, TransactionArguments, TransactionOperation, TransactionStatus } = require("fireblocks-sdk");
const { EthersBridge, Chain } = require("fireblocks-defi-sdk");
const { ethers, PopulatedTransaction, utils } = require("ethers");
const { exit } = require("process");
require('dotenv').config();

const apiSecret = fs.readFileSync(process.env.FIREBLOCKS_SECRET_PATH, "utf8");
const apiKey = process.env.FIREBLOCKS_API_KEY;
const fireblocksApiClient = new FireblocksSDK(apiSecret, apiKey, "https://sandbox-api.fireblocks.io");

const arg = process.argv.slice(2);
 
arg[1] = Number(arg[1]);    //vault number
arg[2] = arg[2];            //asset ID
arg[3] = arg[3];            //spender address
arg[4] = Number(arg[4]);    //approval amount

const main = async() => {

    //retireve asset details from ID
    const getAssetDetails = async(assetId) => {
        const supportedAssets = await fireblocksApiClient.getSupportedAssets();
        for (const asset of supportedAssets) {
            if (asset.id === assetId) {
                return asset;
            }
        }

        console.log("Could not find an asset with that ID.");
        exit(1);
    }

    //retieve the wallet address of the specified asset in the specified vaul
    const getWalletAddress = async(vaultId, assetId) => {
        const depositAddresses = await fireblocksApiClient.getDepositAddresses(vaultId, assetId);
        return depositAddresses[0].address;
    }

    //map base asset ID to chain name
    const toChainId = (assetId) => {
        switch (assetId) {
            case "ETH-AETH":
                return Chain.ARBITRUM;

            case "AVAX":
                return Chain.AVALANCHE

            case "BNB_BSC":
                return Chain.BSC;

            case "BNB_TEST":
                return Chain.BSC_TEST;

            case "FTM":
                return Chain.FANTOM;

            case "ETH_TEST3":
                return Chain.GOERLI;

            case "ETH_TEST2":
                return Chain.KOVAN;

            case "ETH":
                return Chain.MAINNET;

            case "MATIC_POLYGON_MUMBAI":
                return Chain.MUMBAI;

            case "MATIC_POLYGON":
                return Chain.POLYGON;

            case "ETH_TEST4":
                return Chain.RINKEBY;

            case "ETH_TEST":
                return Chain.ROPSTEN;

            default:
                console.log("Unsupported chain.");
                exit(2);
        }
    }

    const assetDetails = await getAssetDetails(arg[2]);

    console.log("Initializing Fireblocks <> Ethers bridge...");
    const bridge = new EthersBridge({ 
        fireblocksApiClient,
        vaultAccountId: String(arg[1]) || "0",
        chain: toChainId(assetDetails.nativeAsset)
    });

    const walletAddress = await getWalletAddress(arg[1], arg[2]);
    const spenderAddress = ethers.utils.getAddress(arg[3]);

    const ERC20_ABI=[ { "constant":true, "inputs":[], "name":"name", "outputs":[ { "name":"", "type":"string" } ], "payable":false, "stateMutability":"view", "type":"function" }, { "constant":false, "inputs":[ { "name":"_spender", "type":"address" }, { "name":"_value", "type":"uint256" } ], "name":"approve", "outputs":[ { "name":"", "type":"bool" } ], "payable":false, "stateMutability":"nonpayable", "type":"function" }, { "constant":true, "inputs":[], "name":"totalSupply", "outputs":[ { "name":"", "type":"uint256" } ], "payable":false, "stateMutability":"view", "type":"function" }, { "constant":false, "inputs":[ { "name":"_from", "type":"address" }, { "name":"_to", "type":"address" }, { "name":"_value", "type":"uint256" } ], "name":"transferFrom", "outputs":[ { "name":"", "type":"bool" } ], "payable":false, "stateMutability":"nonpayable", "type":"function" }, { "constant":true, "inputs":[], "name":"decimals", "outputs":[ { "name":"", "type":"uint8" } ], "payable":false, "stateMutability":"view", "type":"function" }, { "constant":true, "inputs":[ { "name":"_owner", "type":"address" } ], "name":"balanceOf", "outputs":[ { "name":"balance", "type":"uint256" } ], "payable":false, "stateMutability":"view", "type":"function" }, { "constant":true, "inputs":[], "name":"symbol", "outputs":[ { "name":"", "type":"string" } ], "payable":false, "stateMutability":"view", "type":"function" }, { "constant":false, "inputs":[ { "name":"_to", "type":"address" }, { "name":"_value", "type":"uint256" } ], "name":"transfer", "outputs":[ { "name":"", "type":"bool" } ], "payable":false, "stateMutability":"nonpayable", "type":"function" }, { "constant":true, "inputs":[ { "name":"_owner", "type":"address" }, { "name":"_spender", "type":"address" } ], "name":"allowance", "outputs":[ { "name":"", "type":"uint256" } ], "payable":false, "stateMutability":"view", "type":"function" }, { "payable":true, "stateMutability":"payable", "type":"fallback" }, { "anonymous":false, "inputs":[ { "indexed":true, "name":"owner", "type":"address" }, { "indexed":true, "name":"spender", "type":"address" }, { "indexed":false, "name":"value", "type":"uint256" } ], "name":"Approval", "type":"event" }, { "anonymous":false, "inputs":[ { "indexed":true, "name":"from", "type":"address" }, { "indexed":true, "name":"to", "type":"address" }, { "indexed":false, "name":"value", "type":"uint256" } ], "name":"Transfer", "type":"event" } ];
    const contract = new ethers.Contract(assetDetails.contractAddress, ERC20_ABI,
        ethers.getDefaultProvider(toChainId(assetDetails.nativeAsset)));

    //retrieve the approved amount
    const getApproval = async() => {
        if (assetDetails.nativeAsset !== "ETH" && assetDetails.nativeAsset !== "ETH_TEST3") {
            console.log("Reading allowance is currently only supported for Ethereum Mainnet and Goerli.");
            exit(3);
        }
        let approvedAmount = await contract.allowance(walletAddress, spenderAddress);
        console.log("Approved amount: " + approvedAmount);
    }

    //set a new approved amount
    const setApproval = async() => {
        const approvalAmount = arg[4];
        let tx = await contract.populateTransaction.approve(spenderAddress, approvalAmount);
        await processTransaction(bridge, tx);
    }

    async function processTransaction(bridge, tx) {
        const res = await bridge.sendTransaction(tx);
        console.log("Waiting for the transaction to be signed and mined...");
        const txHash = await bridge.waitForTxHash(res.id);
        console.log(`Transaction ${res.id} has been broadcast. TX Hash is ${txHash}`);
    }

    switch (arg[0]) {
        case 'get_approval':
            console.log(`Getting approval amount for asset: ${arg[2]}, spender address: ${arg[3]}, for vault: ${arg[1]}...`);
            getApproval();
            break;
    
        case 'set_approval':
            console.log(`Setting approval amount for asset: ${arg[2]}, spender address: ${arg[3]}, for vault: ${arg[1]}, to amount: ${arg[4]}...`);
            setApproval();
            break;
    
        default:
            console.log(`operation cannot be performed!`);
    }
}

main();